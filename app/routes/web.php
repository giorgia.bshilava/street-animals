<?php
 
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LocaleController;
use App\Http\Controllers\Auth\SignUpController;
use App\Http\Controllers\Auth\SignInController;
use App\Http\Controllers\Auth\AccountController;

Route::get('locale/{lang}', [LocaleController::class,'setLocale']);

Route::get('/', function () {
    return view('main');
});
Route::get('/admin', function () {
    return view('admin');
}); 

// Sign-Up Routes
Route::get('/sign-up', [SignUpController::class, 'showSignUpForm'])->name('sign-up.form');
Route::post('/sign-up', [SignUpController::class, 'signUp'])->name('sign-up.post');

// Sign-In Routes
Route::get('/sign-in', [SignInController::class, 'showSignInForm'])->name('sign-in.form');
Route::post('/sign-in', [SignInController::class, 'signIn'])->name('sign-in.post');

// In routes/web.php
Route::get('/my-account', [AccountController::class, 'index'])->name('my-account');

 
Route::post('sign-out', [SignInController::class, 'signOut'])->name('sign-out');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
 
//added profile_settings.blade.php
Route::get('/profile-settings', function () {
    return view('profile_settings');
})->name('profile.settings');



 
require __DIR__.'/auth.php';