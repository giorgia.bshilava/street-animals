<?php 

return [

	"localization" => "Localization",
	"en" => "EN",
	"ge" => "ქარ",
	"sign_in" => "Sign In",
	"sign_up" => "Sign Up",
	"agencyadress" => "0178, Tbilisi, David Guramishvili av., N5",
	"website" => "Agency Website",
	"contact_email" => "info@ama.gov.ge",
	"contact_phone" => "+(+995) 32 2421424",
	"copyright" => "© All rights are reserved",
	"first_name" => "First Name",
	"last_name" => "Last Name",
	"id_number" => "ID Number",
	"email" => "Email",
	"password" => "Password",
	"confirm_password" => "Confirm Password",
	"submit" => "Submit",
	"already_have_an_account?" => "Already have an account?",
    "not_registetered_yet?"  =>  "Not registered yet?",
	"registration" => "Registration",
	"forgot" => "Forgot password?",
	"recover" => " Recover it",
	"report" => "Report",
	"adress" => "Adress",
	"comments" => "Comments",
	"animal type" => "Animal type",
	"animal count" => "Count",
	"vaccination" => "Vacctinated?",
	"hazardhuman" => "Is it dangerous for human?",
	"otheroption" => "Other ( write in the comments )",
	"dogoption" => "Dog",
	"catoption" => "Cat",
	"yesoption" => "Yes",
	"nooption" => "No",
	"adress1" => "Adress",
	"restriction" => "Sorry, we currently operate only within the city of Tbilisi",
	"welcome" => "Hello",
	"please_sign_in" => "Please sign in to report",
	"not_you" => "Not you?",
	"sign_out" => "Sign Out",
	"my_account" => "My Account:",




]





?>