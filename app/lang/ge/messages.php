<?php 

return [

	"localization" => "ლოკალიზაცია",
	"en" => "EN",
	"ge" => "ქარ",
	"sign_in" => "შესვლა",
	"sign_up" => "რეგისტრაცია",
	"agencyadress" => "0178, თბილისი, დავით გურამიშვილის გამზირი, ჩიხი N5",
	"website" => "სააგენტოს ვებგვერდი",
	"contact_email" => "info@ama.gov.ge",
	"contact_phone" => "+(+995) 32 2421424",
	"copyright" => "© ყველა უფლება დაცულია",
	"first_name" => "სახელი",
	"last_name" => "გვარი",
	"id_number" => "პირადი ნომერი",
	"email" => "ელექტრონული ფოსტა",
	"password" => "პაროლი",
	"confirm_password" => "დაადასტურეთ პაროლი",
	"submit" => "შემდეგი",
	"already_have_an_account?" => "უკვე დარეგისტრირებული ხარ?",
    "not_registetered_yet?" => "არ ხართ დარეგისტრირებული?",
	"registration" => "რეგისტრაცია",
    "forgot" => "პაროლი დაგავიწყდა?",
	"recover" => "აღდგენა",
	"report" => "რეპორტი",
	"adress" => "ბისამართი",
	"comments" => "კომენტარები",
	"animal type" => "რა ცხოველი არის?",
	"animal count" => "რაოდენობა",
	"vaccination" => "ვაკქინირებული არის?",
	"hazardhuman" => "ადამიანისთვის სახიფათოა?",
	"otheroption" => "სხვა(კომენტარებში მიუთითეთ)",
	"dogoption" => "ძაღლი",
	"catoption" => "კატა",
	"yesoption" => "კი",
	"nooption" => "არა",
	"adress1" => "მისამართი",
	"restriction" => "ბოდიშს გიხდით, ამჟამად ჩვენ მარტო თბილისის ფარგლეფში ვმუშაობთ",
	"welcome" => "გამარჯობა",
	"please_sign_in" => "გთხოვთ, შეხვიდეთ თქვენს ანგარიშზე",
	"not_you" => "არასწორი ანგარიში?",
	"sign_out" => "გასვლა",
	"my_account" => "ჩემი ანგარიში: ",
]





?>