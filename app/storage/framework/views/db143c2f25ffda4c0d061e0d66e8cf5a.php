 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="<?php echo e(asset('css/font_style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/sign_in_style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/header_style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/footer_style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/sign_up_style.css;')); ?>">
</head>

<body>

 <!-- header -->
 <?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
 <!-- header -->
    

    <!-- main -->

    <div class="registration-form">
        <p class="title">
            <?php echo app('translator')->get('messages.sign_up'); ?>
        </p>
        
        <form action="<?php echo e(route('sign-up.post')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <form action="<?php echo e(route('sign-up.post')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="input">
                    <!-- First Name -->
                    <input type="text" placeholder="<?php echo app('translator')->get('messages.first_name'); ?>" name="first-name" value="<?php echo e(old('first-name')); ?>" required>
                    <?php $__errorArgs = ['first-name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <!-- Last Name -->
                    <input type="text" placeholder="<?php echo app('translator')->get('messages.last_name'); ?>" name="last-name" value="<?php echo e(old('last-name')); ?>" required>
                    <?php $__errorArgs = ['last-name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <!-- ID Number -->
                    <input type="text" placeholder="<?php echo app('translator')->get('messages.id_number'); ?>" name="id-number" pattern="[0-9]+" title="Please enter only numbers" value="<?php echo e(old('id-number')); ?>" required>
                    <?php $__errorArgs = ['id-number'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <!-- Email -->
                    <input type="email" placeholder="<?php echo app('translator')->get('messages.email'); ?>" name="email" value="<?php echo e(old('email')); ?>" required>
                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <!-- Password -->
                    <input type="password" placeholder="<?php echo app('translator')->get('messages.password'); ?>" name="password" required>
                    <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <!-- Confirm Password -->
                    <input type="password" placeholder="<?php echo app('translator')->get('messages.confirm_password'); ?>" name="password_confirmation" required>
                    <?php $__errorArgs = ['password_confirmation'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="error"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            
                    <button type="submit"><?php echo app('translator')->get('messages.submit'); ?></button>
                </div>
            </form>
            
        </form>

        <p><?php echo app('translator')->get('messages.already_have_an_account?'); ?><a href="sign-in"><?php echo app('translator')->get('messages.sign_in'); ?></a></p>
    </div>

    <!-- main -->


    <!-- footer -->
    <?php echo $__env->make('footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- footer -->
   


</body>


</html>
<?php /**PATH C:\xampp\htdocs\street-animals\app\resources\views/sign_up.blade.php ENDPATH**/ ?>