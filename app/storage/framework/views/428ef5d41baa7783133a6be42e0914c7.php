<?php $__env->startSection('header'); ?>

    <div class="header">
        <div class="logo">
            <a href="/"><img src="http://ama.gov.ge/assets/images/animal-logo.png" alt="Company Logo"></a>
        </div>
        <div class="header-buttons">
            <div class="user-actions">
                <?php if(auth()->guard()->check()): ?>
                    <div class="user-name">
                        <a href="<?php echo e(route('my-account')); ?>">
                            <p><?php echo e(Auth::user()->first_name); ?>!</p>
                        </a>
                    </div>
                    <div class="sign-out">
                        <form id="signout-form" action="<?php echo e(route('sign-out')); ?>" method="POST" style="display: inline;">
                            <?php echo csrf_field(); ?>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('signout-form').submit();">
                                <p><?php echo app('translator')->get('messages.sign_out'); ?></p>
                            </a>
                        </form>
                    </div>
                <?php endif; ?>

                <?php if(auth()->guard()->guest()): ?>
                    <div class="sign-in">
                        <a href="<?php echo e(route('sign-in.form')); ?>">
                            <p>
                                <?php echo app('translator')->get('messages.sign_in'); ?>
                            </p>
                        </a>
                    </div>
                    <div class="sign-up">
                        <a href="<?php echo e(route('sign-up.form')); ?>">
                            <p>
                                <?php echo app('translator')->get('messages.sign_up'); ?>
                            </p>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

            <div class="language-buttons">
                <div class="ENG">
                    <a href="/locale/en">
                        <p>
                            <?php echo app('translator')->get('messages.en'); ?>
                        </p>
                    </a>
                </div>

                <div class="GE">
                    <a href="/locale/ge">
                        <p>
                            <?php echo app('translator')->get('messages.ge'); ?>
                        </p>
                    </a>
                </div>

            </div>


        </div>
    </div>
 
<?php /**PATH C:\xampp\htdocs\street-animals\app\resources\views/header.blade.php ENDPATH**/ ?>