<div class="footer">
        <div class="contact-info-left">
            <div class="email">
                <p><a href='mailto:<?php echo app('translator')->get('messages.contact_email'); ?>'><?php echo app('translator')->get('messages.contact_email'); ?></a></p>
            </div>
            <div class="phone">
                <p><a href="tel:<?php echo app('translator')->get('messages.contact_phone'); ?>"><?php echo app('translator')->get('messages.contact_phone'); ?></a></p>
            </div>
        </div>

        <div class="contact-info-center">
          
                <p>
                    <?php echo date("Y") ?>
                </p>
                <p><?php echo app('translator')->get('messages.copyright'); ?></p>
              
          
        </div>
        <div class="contact-info-right">
            <div class="address">
                <p><a href="https://maps.app.goo.gl/s7df6ESTkNgyipJR9"><?php echo app('translator')->get('messages.agencyadress'); ?></a></p>
            </div>
            <div class="website">
                <p><a href="http://ama.gov.ge/" style="width: 225px"><?php echo app('translator')->get('messages.website'); ?></a></p>
            </div>
        </div>


    </div><?php /**PATH C:\xampp\htdocs\street-animals\app\resources\views/footer.blade.php ENDPATH**/ ?>