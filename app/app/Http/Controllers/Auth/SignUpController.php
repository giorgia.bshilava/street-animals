<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SignUpController extends Controller
{
    public function showSignUpForm()
    {
        return view('sign_up');
    }

    public function signUp(Request $request)
{
    \Log::info('Sign up form submitted', $request->all()); // Confirm form submission

    // Validate and log if validation fails
    try {
        $this->validator($request->all())->validate();
    } catch (\Illuminate\Validation\ValidationException $e) {
        \Log::error('Validation failed', $e->errors());
        return redirect()->back()->withErrors($e->errors())->withInput();
    }

    \Log::info('Validation passed'); // Confirm validation success

    // Create and log the user creation
    try {
        $user = $this->create($request->all());
        \Log::info('User created', ['user_id' => $user->id]);
    } catch (\Exception $e) {
        \Log::error('User creation failed', ['error' => $e->getMessage()]);
        return redirect()->back()->with('error', 'User creation failed');
    }

    // Optional: Automatically log in the user after sign-up
    auth()->login($user);

    return redirect()->route('sign-in.form'); // Adjust the route as needed
}


    protected function validator(array $data)
{
    return Validator::make($data, [
        'first-name' => ['required', 'string', 'max:255'],
        'last-name' => ['required', 'string', 'max:255'],
        'id-number' => ['required', 'string', 'max:255', 'size:11'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
}

protected function create(array $data)
{
    \Log::info('Creating user with data', $data);

    return User::create([
        'first_name' => $data['first-name'],
        'last_name' => $data['last-name'],
        'id_number' => $data['id-number'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
    ]);
}
}
