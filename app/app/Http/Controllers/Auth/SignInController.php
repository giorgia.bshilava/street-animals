<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignInController extends Controller
{
    public function showSignInForm()
    {
        return view('sign_in'); // Ensure you have a sign_in.blade.php view file
    }

    public function signIn(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/'); // Change this to your intended route
        }

        return redirect()->back()->withErrors(['email' => 'The provided credentials do not match our records.']);
    }
    public function signOut()
    {
        Auth::logout();
        return redirect()->route('sign-in.form');
    }
}
