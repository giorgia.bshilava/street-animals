<div class="footer">
        <div class="contact-info-left">
            <div class="email">
                <p><a href='mailto:@lang('messages.contact_email')'>@lang('messages.contact_email')</a></p>
            </div>
            <div class="phone">
                <p><a href="tel:@lang('messages.contact_phone')">@lang('messages.contact_phone')</a></p>
            </div>
        </div>

        <div class="contact-info-center">
          
                <p>
                    <?php echo date("Y") ?>
                </p>
                <p>@lang('messages.copyright')</p>
              
          
        </div>
        <div class="contact-info-right">
            <div class="address">
                <p><a href="https://maps.app.goo.gl/s7df6ESTkNgyipJR9">@lang('messages.agencyadress')</a></p>
            </div>
            <div class="website">
                <p><a href="http://ama.gov.ge/" style="width: 225px">@lang('messages.website')</a></p>
            </div>
        </div>


    </div>