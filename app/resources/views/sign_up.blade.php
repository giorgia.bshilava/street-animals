 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="{{ asset('css/font_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_in_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_up_style.css;') }}">
</head>

<body>

 <!-- header -->
 @include('header')
 <!-- header -->
    

    <!-- main -->

    <div class="registration-form">
        <p class="title">
            @lang('messages.sign_up')
        </p>
        
        <form action="{{ route('sign-up.post') }}" method="post">
            @csrf
            <form action="{{ route('sign-up.post') }}" method="post">
                @csrf
                <div class="input">
                    <!-- First Name -->
                    <input type="text" placeholder="@lang('messages.first_name')" name="first-name" value="{{ old('first-name') }}" required>
                    @error('first-name')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <!-- Last Name -->
                    <input type="text" placeholder="@lang('messages.last_name')" name="last-name" value="{{ old('last-name') }}" required>
                    @error('last-name')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <!-- ID Number -->
                    <input type="text" placeholder="@lang('messages.id_number')" name="id-number" pattern="[0-9]+" title="Please enter only numbers" value="{{ old('id-number') }}" required>
                    @error('id-number')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <!-- Email -->
                    <input type="email" placeholder="@lang('messages.email')" name="email" value="{{ old('email') }}" required>
                    @error('email')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <!-- Password -->
                    <input type="password" placeholder="@lang('messages.password')" name="password" required>
                    @error('password')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <!-- Confirm Password -->
                    <input type="password" placeholder="@lang('messages.confirm_password')" name="password_confirmation" required>
                    @error('password_confirmation')
                        <div class="error">{{ $message }}</div>
                    @enderror
            
                    <button type="submit">@lang('messages.submit')</button>
                </div>
            </form>
            
        </form>

        <p>@lang('messages.already_have_an_account?')<a href="sign-in">@lang('messages.sign_in')</a></p>
    </div>

    <!-- main -->


    <!-- footer -->
    @include('footer')
    <!-- footer -->
   


</body>


</html>
