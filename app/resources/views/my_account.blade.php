<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>

    <link rel="stylesheet" href="{{ asset('css/font_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_in_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_up_style.css') }}">
</head>

<body>

<!-- header -->
@include('header')
<!-- header -->


<!-- Main content -->
<div class="account-info">
	<h1>@lang('messages.my_account')</h1>
	<ul>
		<li><strong>@lang('messages.first_name'): </strong>{{ $user->first_name }}</li>
		<li><strong>@lang('messages.last_name'): </strong>{{ $user->last_name }}</li>
		<li><strong>@lang('messages.id_number'): </strong>{{ $user->id_number }}</li>
		<li><strong>@lang('messages.email'): </strong>{{ $user->email }}</li>
	</ul>
</div>
<!-- Main content -->

<!-- footer -->
@include('footer')
<!-- footer -->

</body>
</html>
