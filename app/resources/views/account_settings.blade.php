<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile Settings</title>

    <link rel="stylesheet" href="{{ asset('css/font_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/profile_style.css') }}">
</head>

<body>

    <!-- header -->
    @include('header')
    <!-- header -->

    <div class="profile-settings-form">
        <p class="title">
            @lang('messages.my_account')
        </p>
        
        <form action="#" method="post">
            @csrf
            <!-- Profile Settings Form -->
            <div>
                <span>Giorgi Kenjia</span>
                <img src="{{ asset('images/edit-icon.png') }}" class="edit-icon" alt="Edit profile">
            </div>

            <div>
                <span>Gio.Kenjia@gmail.com</span>
                <img src="{{ asset('images/edit-icon.png') }}" class="edit-icon" alt="Edit email">
            </div>

            <div>
                <span>12345678910</span>
                <img src="{{ asset('images/edit-icon.png') }}" class="edit-icon" alt="Edit phone number">
            </div>

            <div>
                <span>+995599874811</span>
                <img src="{{ asset('images/edit-icon.png') }}" class="edit-icon" alt="Edit secondary phone number">
            </div>

            <div>
                <span>********</span>
                <img src="{{ asset('images/edit-icon.png') }}" class="edit-icon" alt="Edit secondary phone number">
            </div>
            <div>
                <img src="{{ asset('images/save-icon.png') }}" class="save-icon" alt="Save secondary phone number">
                <img src="{{ asset('images/delete-icon.png') }}" class="delete-icon" alt="Edit secondary phone number">
            </div>
        </form>
    </div>

    <!-- footer -->
    @include('footer')
    <!-- footer -->

</body>

</html>
