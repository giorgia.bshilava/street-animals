<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Main</title>
    <link rel="stylesheet" href="{{ asset('css/main_page_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/map_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font_style.css') }}">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="{{ asset('js/map.js') }}"></script>
    <script src="{{ asset('js/form.js') }}"></script>
    
        
</head>

<body>
    <script>
        var localizationStrings = {!! json_encode([
            'restriction' => __('messages.restriction'),
        ]) !!};
    </script>
    <!-- header -->
    @include('header')
    <!-- header -->
    <button class="report-menu-toggle">
  <span class="line"></span>
  <span class="line"></span>
  <span class="line"></span>
</button>

    <div class="main-container">
        <div class="report">
            <div class="title">
                <!-- <div class="welcome">
                    @auth
                    @else
                        <p>@lang('messages.please_sign_in')</p>
                        <a href="{{ route('sign-in.form') }}">@lang('messages.sign_in')</a>
                    @endauth
                </div> -->
                <!-- sign in gaket figmas ren tesh -->
                @lang('messages.report')
            </div>
            <div class="inputs">
                <label for="animalTypeDropdown"></label>
                <select class="dropdown-box" id="animalTypeDropdown" onchange="checkOther(this)">
                    <option value="" disabled selected hidden>@lang('messages.animal type')</option>
                    <option value="option1">@lang('messages.dogoption')</option>
                    <option value="option2">@lang('messages.catoption')</option>
                    <option value="option3">@lang('messages.otheroption')</option>
                </select>
                
                <label for="animalCountDropdown"></label>
                <select class="dropdown-box" id="animalCountDropdown" onchange="checkOther(this)">
                    <option value="" disabled selected hidden>@lang('messages.animal count')</option>
                    <option value="option1">1</option>
                    <option value="option2">2</option>
                    <option value="option3">@lang('messages.otheroption')</option>
                </select>
                
                <label for="vaccinationDropdown"></label>
                <select class="dropdown-box" id="vaccinationDropdown" onchange="checkOther(this)">
                    <option value="" disabled selected hidden>@lang('messages.vaccination')</option>
                    <option value="option1">@lang('messages.yesoption')</option>
                    <option value="option2">@lang('messages.nooption')</option>
                </select>
                
                <label for="hazardHumanDropdown"></label>
                <select class="dropdown-box" id="hazardHumanDropdown" onchange="checkOther(this)">
                    <option value="" disabled selected hidden>@lang('messages.hazardhuman')</option>
                    <option value="option1">@lang('messages.yesoption')</option>
                    <option value="option2">@lang('messages.nooption')</option>
                </select>
                
                <div class="title">@lang('messages.adress1')</div>
                <input type="text" id="addressLine" name="address">
                <input type="comments" placeholder='@lang('messages.comments')' name="comments">
                <input type="text" id="otherInput" placeholder="Specify other option" style="display: none;">
            </div>
            
            <button type="submit">@lang('messages.report')</button>
        </div>
        <div id="map"></div>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM4uqenRG6y3MfvcaqnN9K7polTmttfN4&callback=initMap&v=weekly&solution_channel=GMP_CCS_geocodingservice_v1"
            defer></script>
    </div>
    <!-- footer -->
    @include('footer')
    <!-- footer -->
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var toggleButton = document.getElementById('toggleReportButton');
            var reportDiv = document.querySelector('.report');
            toggleButton.addEventListener('click', function () {
                reportDiv.classList.toggle('visible');
            });
        });
    </script>
</body>

</html>
