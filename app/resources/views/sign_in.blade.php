<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>

    <link rel="stylesheet" href="{{ asset('css/font_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_in_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/header_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sign_up_style.css') }}">
</head>

<body>

    <!-- header -->
    @include('header')
    <!-- header -->

    <!-- main -->
    <div class="main-container">
        <div class="registration-form">
            <p class="title">
                @lang('messages.sign_in')
            </p>
            <form action="{{ route('sign-in.post') }}" method="post">
                @csrf
                <!-- Sign-In Form -->

                <div>
                    <x-text-input placeholder="{{ __('messages.email') }}" id="email" type="email" name="email"
                        :value="old('email')" required autofocus autocomplete="username" />
                    <x-input-error :messages="$errors->get('email')" />
                </div>

                <div>
                    <input type="password" placeholder='@lang('messages.password')' name="password" required>
                    <x-input-error :messages="$errors->get('password')" />
                </div>

                <button type="submit">@lang('messages.submit')</button>
            </form>
            <div class="alternative-options">
                <p>@lang('messages.not_registetered_yet?')<a href="{{ route('sign-up.form') }}">@lang('messages.sign_up')</a></p>
                <p>@lang('messages.forgot')<a href="">@lang('messages.recover')</a></p>
            </div>
        </div>
    </div>

    <!-- footer -->
    @include('footer')
    <!-- footer -->

</body>

</html>
