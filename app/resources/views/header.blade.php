@section('header')

    <div class="header">
        <div class="logo">
            <a href="/"><img src="http://ama.gov.ge/assets/images/animal-logo.png" alt="Company Logo"></a>
        </div>
        <div class="header-buttons">
            <div class="user-actions">
                @auth
                    <div class="user-name">
                        <a href="{{ route('my-account') }}">
                            <p>{{ Auth::user()->first_name }}!</p>
                        </a>
                    </div>
                    <div class="sign-out">
                        <form id="signout-form" action="{{ route('sign-out') }}" method="POST" style="display: inline;">
                            @csrf
                            <a href="#" onclick="event.preventDefault(); document.getElementById('signout-form').submit();">
                                <p>@lang('messages.sign_out')</p>
                            </a>
                        </form>
                    </div>
                @endauth

                @guest
                    <div class="sign-in">
                        <a href="{{ route('sign-in.form') }}">
                            <p>
                                @lang('messages.sign_in')
                            </p>
                        </a>
                    </div>
                    <div class="sign-up">
                        <a href="{{ route('sign-up.form') }}">
                            <p>
                                @lang('messages.sign_up')
                            </p>
                        </a>
                    </div>
                @endguest
            </div>

            <div class="language-buttons">
                <div class="ENG">
                    <a href="/locale/en">
                        <p>
                            @lang('messages.en')
                        </p>
                    </a>
                </div>

                <div class="GE">
                    <a href="/locale/ge">
                        <p>
                            @lang('messages.ge')
                        </p>
                    </a>
                </div>

            </div>


        </div>
    </div>
 
