/**
 * @license
 * Copyright 2019 Google LLC. All Rights Reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
var map; // Declare map variable globally
var marker; // Declare marker variable globally
var circle; // Declare circle variable globally
var tbilisiBounds; // Declare variable to store Tbilisi city boundary
 
function initMap() {
    // Check if google is defined and the map can be created
    if (typeof google === 'undefined' || typeof google.maps === 'undefined') {
        console.error('Google Maps API not loaded');
        return;
    }

    // Define the boundary of Tbilisi city
    tbilisiBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(41.5939, 44.6900), // Southwest corner of Tbilisi
        new google.maps.LatLng(41.8600, 44.9731)  // Northeast corner of Tbilisi
    );

    // Initialize map centered on Tbilisi
    var tbilisiCenter = { lat: 41.7151, lng: 44.8271 };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: tbilisiCenter,
        mapTypeControl: false // Remove map type control buttons (satellite, terrain)
    });

    // Add event listener for map click
    map.addListener('click', function(event) {
        // Check if the clicked location is within the boundary of Tbilisi
        if (tbilisiBounds.contains(event.latLng)) {
            placeMarker(event.latLng);
            getAddress(event.latLng);
            drawCircle(event.latLng);
        } else {
            alert(localizationStrings.restriction);
        }
    });
}

// Function to place a marker at the clicked location
function placeMarker(location) {
    // Remove existing marker, if any
    if (marker) {
        marker.setMap(null);
    }

    // Create a new marker at the clicked location
    marker = new google.maps.Marker({
        position: location,
        map: map
    });
}

// Function to get the address of the clicked location
function getAddress(location) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'location': location }, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                // Display the address in the address input field
                document.getElementById('addressLine').value = results[0].formatted_address;
            } else {
                console.error('No address found');
            }
        } else {
            console.error('Geocoder failed due to: ' + status);
        }
    });
}

// Function to draw a circle around the marker
function drawCircle(center) {
    // Remove existing circle, if any
    if (circle) {
        circle.setMap(null);
    }

    // Create a new circle with a radius of 100 meters
    circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: center,
        radius: 100
    });
}
window.initMap = initMap;
